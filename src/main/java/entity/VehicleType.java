package entity;

public enum VehicleType {

    Sedan, Hatchback, SUV
}

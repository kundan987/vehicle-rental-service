package service;

import entity.VehicleType;

import java.util.Date;

public interface VRSService {

    void addBranch(String branchName);

    void allocatePrice(String branchName, VehicleType vehicleType, Integer price);

    void addVehicle(String vehicleId, VehicleType vehicleType, String branchName);

    void bookVehicle(VehicleType vehicleType, Date startTime, Date endTime);
}

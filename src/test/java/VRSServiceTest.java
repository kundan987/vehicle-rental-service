import entity.VehicleType;
import org.junit.Before;
import org.junit.Test;
import service.VRSService;
import service.VRSServiceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VRSServiceTest {

    VRSService service;

    public static final String basant = "Basant Vihar";
    public static final String cyber = "Cyber City";
    @Before
    public void setup(){
        service = new VRSServiceImpl("LowestPrice");
    }

    @Test
    public void addBranch() throws ParseException {
        service.addBranch(basant);
        service.addBranch(cyber);
        service.allocatePrice(basant, VehicleType.Sedan, 100);
        service.allocatePrice(basant, VehicleType.Hatchback, 80);
        service.allocatePrice(cyber, VehicleType.Sedan, 200);
        service.allocatePrice(cyber, VehicleType.Hatchback, 50);
        service.addVehicle("DL 01 MR 9310", VehicleType.Sedan, basant);
        service.addVehicle("DL 01 MR 9311", VehicleType.Sedan, cyber);
        service.addVehicle("DL 01 MR 9312", VehicleType.Hatchback, cyber);
        String pattern = "DD-MM-YYYY HH:mm a";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        service.bookVehicle(VehicleType.Sedan, simpleDateFormat.parse("29-02-2020 10:00 AM"), simpleDateFormat.parse("29-02-2020 01:00 PM"));
    }

    @Test
    public void allocatePrice(){

    }

    @Test
    public void addVehicle(){

    }

    @Test
    public void bookVehicle(){

    }
}

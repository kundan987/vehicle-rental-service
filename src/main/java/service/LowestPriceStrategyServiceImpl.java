package service;

import entity.Interval;
import entity.VehicleType;

import java.util.*;

public class LowestPriceStrategyServiceImpl implements RentalStrategyService{


    @Override
    public void bookVehicle(VehicleType vehicleType, Date startTime, Date endTime, Map<String, Map<VehicleType, List<String>>> inventory,
                            Map<String, Map<VehicleType, Integer>> priceMap, Map<String, List<Interval>> bookingMap) {
        String branchName = getBranchwithLowestPrice(priceMap, vehicleType, 0);
        Map<VehicleType, List<String>> vehicleTypeInventoryMap = inventory.get(branchName);
        List<String> vehicleList = vehicleTypeInventoryMap.get(vehicleType);

        for(String vehicle : vehicleList){
            List<Interval> bookedSlot = bookingMap.get(vehicle);
            if(checkOverLapBookingExist(bookedSlot, new Interval(startTime, endTime))){

            } else {
                System.out.println(vehicle + " from " + branchName + " Booked");
                bookedSlot.add(new Interval(startTime, endTime));
                bookingMap.put(vehicle, bookedSlot);
                return;
            }
        }

    }

    private boolean checkOverLapBookingExist(List<Interval> bookinglist, Interval interval) {

        Collections.sort(bookinglist, (a, b)-> (int)a.getStartTime().getTime() - (int)b.getEndTime().getTime());
        int i=0;
        while(i<bookinglist.size() && bookinglist.get(i).getEndTime().getTime()< interval.getStartTime().getTime()){
            i++;
        }
        if(i==bookinglist.size())
            return false;
        if(bookinglist.get(i).getStartTime().getTime()>interval.getEndTime().getTime())
            return false;
        return true;
    }

    String getBranchwithLowestPrice(Map<String, Map<VehicleType, Integer>> priceMap, VehicleType vehicleType, int minPrice){
        String branchName = null;
        for(Map.Entry<String, Map<VehicleType, Integer>> map : priceMap.entrySet()){

            Map<VehicleType, Integer> vehicleTypePrice = map.getValue();

            int price =  vehicleTypePrice.get(vehicleType);
            if(price <= minPrice){
                minPrice = price;
                branchName = map.getKey();
            }
        }
        return branchName;
    }
}

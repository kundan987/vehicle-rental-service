package service;

import entity.Interval;
import entity.VehicleType;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface RentalStrategyService {

    void bookVehicle(VehicleType vehicleType, Date startTime, Date endTime, Map<String, Map<VehicleType, List<String>>> inventory, Map<String, Map<VehicleType, Integer>> priceMap, Map<String, List<Interval>> bookings);
}

package factory;

import service.LowestPriceStrategyServiceImpl;
import service.RentalStrategyService;

public class StrategyFactory {

    RentalStrategyService rentalStrategyService;

    public RentalStrategyService getStrategy(String strategy){
        if(strategy.equals("LowestPrice"))
            rentalStrategyService = new LowestPriceStrategyServiceImpl();
        else
            rentalStrategyService = null;

        return rentalStrategyService;

    }
}

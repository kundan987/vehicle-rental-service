package service;

import entity.Interval;
import entity.VehicleType;
import factory.StrategyFactory;

import java.text.SimpleDateFormat;
import java.util.*;

public class VRSServiceImpl implements VRSService{

    Map<String, Map<VehicleType, List<String>>> inventory;
    Map<String, Map<VehicleType, Integer>> priceMap;

    Map<String, List<Interval>> bookings;



    RentalStrategyService rentalStrategyService;

    public VRSServiceImpl(String strategy) {
        inventory =  new HashMap<>();
        priceMap = new HashMap<>();
        bookings = new HashMap<>();
        StrategyFactory strategyFactory = new StrategyFactory();
        rentalStrategyService = strategyFactory.getStrategy(strategy);
    }

    @Override
    public void addBranch(String branchName) {
        if (inventory.containsKey(branchName))
            System.out.println("Branch already Added");
        inventory.put(branchName, new HashMap<>());
        priceMap.put(branchName, new HashMap<>());
    }

    @Override
    public void allocatePrice(String branchName, VehicleType vehicleType, Integer price) {
        if (!priceMap.containsKey(branchName))
            System.out.println("Branch not Added");
        Map<VehicleType, Integer> vehicleTypePriceMap = priceMap.get(branchName);
        if(vehicleTypePriceMap == null)
            vehicleTypePriceMap = new HashMap<>();
        vehicleTypePriceMap.put(vehicleType, price);
        priceMap.put(branchName, vehicleTypePriceMap);

    }

    @Override
    public void addVehicle(String vehicleId, VehicleType vehicleType, String branchName) {
        if (!inventory.containsKey(branchName))
            System.out.println("Branch not Added");
        Map<VehicleType, List<String>> vehicleTypeListMap = inventory.get(branchName);
        List<String> vehicleList;
        if (vehicleTypeListMap.containsKey(vehicleType)){
            vehicleList = vehicleTypeListMap.get(vehicleType);
        } else {
            vehicleList = new ArrayList<>();
        }
        vehicleList.add(vehicleId);
        vehicleTypeListMap.put(vehicleType, vehicleList);
        inventory.put(branchName, vehicleTypeListMap);
    }

    @Override
    public void bookVehicle(VehicleType vehicleType, Date startTime, Date endTime) {

        rentalStrategyService.bookVehicle(vehicleType, startTime, endTime, inventory, priceMap, bookings);
    }
}
